#!/usr/bin/ruby

require 'mongo'

Mongo::Logger.logger.level = ::Logger::FATAL

begin

    #1 Conexion y algunas credenciales basicas para la misma
    client = Mongo::Client.new(['127.0.0.1:27017'], :database => 'testPeliculas',
                                                    :server_selection_timeout => 5)

    #2 traer todas las colecciones                                                    
    client.collections.each{|coll|puts coll.name}
    
    #3 traer columnas del documento peliculas
    client[:peliculas].find.each{|doc| puts doc}

    #4 traer estadisticas   
    db = client.use("testPeliculas")
    db.command({"dbstats" => 1}).documents[0].each do |key, value|
        puts "#{key}: #{value}"
    end

    #5 lectura de datos
    client[:peliculas].find.each{|doc| puts doc}

    #6 contar documentos
    docs = client[:peliculas].find
    puts "hay #{docs.count} documentos"

    #7 Select para traer la pelicula Avengers
    puts ""
    client[:peliculas].find(:name => 'Avengers').each do |doc|
        puts doc
    end

    ##8 Operadores
    # Prueba para precios inferiores a 30.000
    puts client[:peliculas].find("price" => {'$lt' => 30000}).to_a
    puts ""
    # Prueba para precios superiores a 30.000
    puts client[:peliculas].find("price" => {'$gt' => 30000}).each do |doc|
        puts doc
    end

    # 9 operadores $ y $
    puts client[:peliculas].find('$or' => [{:name => "Avengers"}, {:name => "Pan y vino"}]).to_a
    puts ""
    puts client[:peliculas].find('$and' => [{:price => {'$gt' => 20000}},
        {:price => {'$lt' => 50000}}]).to_a

    # 10 Proyecciones
    cursor = client[:peliculas].find({}, {:projection => {:_id => 0}})
    cursor.each{|doc| puts doc}

    # 11 Limitacion de salida de datos
    docs = client[:peliculas].find().skip(2).limit(5)
    docs.each do |doc|
        puts doc
    end

    # 12 Agregaciones
    agr = [{"$group" => {:_id => 1, :all => {"$sum" => "$price"}}}];
    client[:peliculas].aggregate(agr).each{|doc| puts doc}

    # 13 Insert_one
    #doc = {:_id => 9, :name => "El oficial y el espia", :price => 37600}
    #client[:peliculas].insert_one doc

    # 14 Insertar varios documentos
    #result = client[:continents].insert_many([
    #    {:_id => BSON::ObjectId.new, :name => 'Africa'},
    #    {:_id => BSON::ObjectId.new, :name => 'America'},
    #    {:_id => BSON::ObjectId.new, :name => 'Europa'},
    #    {:_id => BSON::ObjectId.new, :name => 'Asia'},
    #])
    #puts "#{result.inserted_count} documentos insertados"

    # 15 Modificar documentos
    client[:peliculas].delete_one({:name => "El oficial y el espia"})
    client[:peliculas].update_one({:name => "Vida y obra"}, '$set' => {:price => 52000})
    
    client.close

rescue Mongo::Error::NoServerAvailable => e

    p "No se puede conectar al servidor por: "
    p e

end